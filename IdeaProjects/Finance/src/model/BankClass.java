package model;

public class BankClass extends NameAndAddress{
   /* int year;

    public BankClass(String name, String address, int year) {
        super(name, address);
        this.year = year;

    }

    int minTerm;
    double percentDeposit;
    public double hrn_usd;
    public double usd_hrn;
    public double hrn_eur;
    public double eur_hrn;
    public double hrn_rub;
    public double rub_hrn;
    int maxSumExchange;
    double echangeCommision;
    int exchangeIntCommision;
    int maxCredit;
    double creditCommision;
    public  double transferCommision;
    int transferIntCommision;
*/
    LombardClass credit1 ;//= new LombardClass(this.name, this.address, this.maxCredit, this.creditCommision);
    PIFClass deposit;// = new PIFClass(this.name, this.address, this.year, minTerm, percentDeposit);
    ExchangeClass exchange;// = new ExchangeClass(name, address,hrn_usd, usd_hrn, hrn_eur, eur_hrn, hrn_rub, rub_hrn, maxSumExchange, echangeCommision, exchangeIntCommision);
    Post transfer;// = new Post(this.name,this.address,this.transferIntCommision, transferCommision);

    public BankClass(String name, String address, LombardClass credit1, PIFClass deposit, ExchangeClass exchange, Post transfer) {
        super(name, address);
        this.credit1 = credit1;
        this.deposit = deposit;
        this.exchange = exchange;
        this.transfer = transfer;
    }

    @Override
    public String toString() {
        return "BankClass{" +
                "credit1=" + credit1 +
                ", deposit=" + deposit +
                ", exchange=" + exchange +
                ", transfer=" + transfer +
                '}';
    }
}
