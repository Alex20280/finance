package model;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ExchangeClass ExchangeObj = new ExchangeClass("Exchange1", "Address1",  26.95, 0.0369, 28.94, 0.034, 2.08, 0.480,20000,0,0);
        ExchangeClass ExchangeObj2 = new ExchangeClass("Exchange2", "Address2", 27.10, 0.0370, 28.95, 0.035, 3.08, 0.580,20000,0,0);
        ExchangeClass Bank2=new ExchangeClass("BankExchnge", "Address9", 25.95, 0.0349, 27.94, 0.033, 2.00, 0.0385,12000,0,15);
        LombardClass LombardObj = new LombardClass("Lombard", "Address3", 50000, 40);
        LombardClass CreditCafe = new LombardClass("CreditCafe", "Address4", 4000, 200);
        LombardClass CreditUnion = new LombardClass("CreditUnion", "Address5", 100000, 20);
        PIFClass PifObj = new PIFClass("Pif", "Address6", 2002, 12, 10);
        PIFClass Bank3=new PIFClass("BankDeposit", "Address10", 2015, 12, 0);
        Post PostObj = new Post("Post", "Address7", 0,2);
        Post Bank1=new Post("BankTransfer", "Address8", 5,1);
        LombardClass Bank4=new LombardClass("BankCredit", "Address11", 200000, 25);
        BankClass bank = new BankClass("name", "Address",Bank4,Bank3,Bank2,Bank1);

        ArrayList<Exchange> exchangeList = new ArrayList<>();
        exchangeList.add(ExchangeObj);
        exchangeList.add(ExchangeObj2);
        exchangeList.add(bank.exchange);

        ArrayList<LombardClass> financeList = new ArrayList<LombardClass>();
        financeList.add(LombardObj);
        financeList.add(CreditCafe);
        financeList.add(CreditUnion);
        financeList.add(bank.credit1);

        ArrayList<Deposit> depositList = new ArrayList<>();
        depositList.add(PifObj);
        depositList.add(bank.deposit);

        ArrayList<Transfer> postList = new ArrayList<>();
        postList.add(PostObj);
        postList.add(bank.transfer);



        LombardClass Lombardbuffer=new LombardClass("Lombard", "Address3", 50000, 0);
        for(int i=0; i<financeList.size()-1; i++){
            if(financeList.get(i).compareTo(financeList.get(i+1))>0)
            {
                Lombardbuffer=financeList.get(i);
            }
        } System.out.println(Lombardbuffer.toString());

        Transfer Postbuffer = new Post("Post", "Address7", 0,2);
        for (int i=0; i<postList.size()-1; i++){
            if (postList.get(i).compareTo(postList.get(i+1))<0)
            {
                Postbuffer=postList.get(i);
            }
        }System.out.println(Postbuffer.toString());

        Deposit Pifbuffer = new PIFClass("Bank3", "Address10", 2015, 1, 0);
        for (int i=0; i<depositList.size()-1; i++){
            if (depositList.get(i).compareTo(depositList.get(i+1))>0)
            {
                Pifbuffer = depositList.get(i);
            }
        } System.out.println(Pifbuffer.toString());

        Exchange Exchangebuffer = new ExchangeClass("BankExchnge", "Address9", 25.95, 0.0349, 27.94, 0.033, 2.00, 0.0385,12000,0,15);
        for (int i=0; i<exchangeList.size()-1;i++){
            if (exchangeList.get(i).compareTo(exchangeList.get(i+1))>0)
            {
                Exchangebuffer=exchangeList.get(i);
            }

        }System.out.println(Exchangebuffer.toString());



    }
}
