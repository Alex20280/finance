package model;

public abstract class NameAndAddress{
    String name;
    String address;

    public NameAndAddress(String name, String address) {
        this.name = name;
        this.address = address;
    }

}
