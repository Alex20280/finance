package model;

public interface Deposit extends Comparable<Deposit> {

    double getPercent();
}
