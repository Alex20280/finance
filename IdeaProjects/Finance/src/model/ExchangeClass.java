package model;

public class ExchangeClass implements Exchange {

    String name;
    String address;
    public double hrn_usd;
    public double usd_hrn;
    public double hrn_eur;
    public double eur_hrn;
    public double hrn_rub;
    public double rub_hrn;
    int maxSumExchange;
    double echangeCommision;
    int exchangeIntCommision;


    public ExchangeClass(String _name, String _address,double _hrn_usd, double _usd_hrn, double _hrn_eur, double _eur_hrn, double _hrn_rub, double _rub_hrn, int maxSumExchange, double echangeCommision, int exchangeIntCommision) {
        this.name = _name;
        this.address = _address;
        this.hrn_usd = _hrn_usd;
        this.usd_hrn = _usd_hrn;
        this.hrn_eur = _hrn_eur;
        this.eur_hrn = _eur_hrn;
        this.hrn_rub = _hrn_rub;
        this.rub_hrn = _rub_hrn;
        this.maxSumExchange = maxSumExchange;
        this.echangeCommision = echangeCommision;
        this.exchangeIntCommision = exchangeIntCommision;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    public String toString() {
        return "ExchangeClass{" +
                "Name='" + name + '\'' +
                ", Adress='" + address + '\'' +
                ", hrn_usd=" + hrn_usd +
                ", usd_hrn=" + usd_hrn +
                ", hrn_eur=" + hrn_eur +
                ", eur_hrn=" + eur_hrn +
                ", hrn_rub=" + hrn_rub +
                ", rub_hrn=" + rub_hrn +
                '}';
    }

}
