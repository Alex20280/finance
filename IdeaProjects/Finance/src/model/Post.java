package model;

public class Post extends NameAndAddress implements Transfer{

    public double comission;
    public int intComission;

    public Post(String _name, String _address, int _intComission, double _comission) {
        super(_name, _address);
        this.intComission=_intComission;
        if (_comission>=1)
            this.comission = _comission/100;
        else this.comission = _comission;
    }

    @Override
    public double transferMoney(double sum) {
        return sum-(sum*comission);
    }

    @Override
    public int compareTo(Transfer o) {
        if(this.transferMoney(this.transferSum)<o.transferMoney(this.transferSum))
        {
            return 1;
        }
        else if(this.transferMoney(this.transferSum)==o.transferMoney(this.transferSum)) return 0;
        else return -1;
    }
    @Override
    public String toString() {
        return "Post{" +
                " Name: " + name+
                ", Address: " + address +
                ", Comission =" + comission +
                '}';
    }
}
