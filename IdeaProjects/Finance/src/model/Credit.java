package model;

public interface Credit extends Comparable<Credit> {

    public double getPersent();
    double getCredit (double sum);
}
