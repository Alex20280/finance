package model;

public class PIFClass extends NameAndAddress implements Deposit {

    int year;
    int minTerm;
    double percent;

    public PIFClass(String _name, String _address, int _year, int _minTerm,double _percent) {
        super(_name, _address);
        this.year = _year;
        this.minTerm = _minTerm;
        if (_percent>=1)
            this.percent = _percent/100;
        else this.percent = _percent;
    }

    /*  @Override
        public double getDeposit(double sum) {
            return sum * this.percent+sum;
        }
    */
    @Override
    public double getPercent() {
        return this.percent;
    }



    @Override
    public int compareTo(Deposit o) {
        if(this.getPercent()>o.getPercent())
        {
            return 1;
        }
        else if(this.getPercent()>o.getPercent()) return 0;
        else return -1;
    }

    @Override
    public String toString() {
        return "PIFClass{" +
                " Name: " + name+
                ", Address: " + address +
                ", Year of foundation= " + year +
                ", Minimum Term= " + minTerm +
                '}';
    }

}
