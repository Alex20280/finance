package model;

public class LombardClass extends NameAndAddress implements Credit {

    int maxsum;
    double percent;
    public LombardClass(String _name, String _address, int _maxsum, double _percent) {
        super(_name, _address);
        this.maxsum = _maxsum;
        if (_percent>=1)
            this.percent=_percent/100;
        else this.percent = _percent;
    }

    @Override
    public double getCredit(double sum) {
        if (maxsum>sum)
        return sum*this.percent+sum;
        else {System.out.println("Мы не даем таких кредитов");
        return 0;
    }

}

    @Override
    public double getPersent() {
        return this.percent;
    }

    @Override
    public int compareTo(Credit o) {
        if(this.getPersent()<o.getPersent())
        {
            return 1;
        }
        else if(this.getPersent()==o.getPersent()) return 0;
        else return -1;
    }

    @Override
    public String toString() {
        return "LombardClass{" +
                " Name: " + name+
                ", Address: " + address +
                ", Maximum sum: " + maxsum +
                ", Percent: " + percent +
                '}';
    }
}
