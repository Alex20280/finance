package model;

public interface Transfer extends Comparable<Transfer>{

    public final double transferSum=20000;
    double transferMoney (double sum);
}
