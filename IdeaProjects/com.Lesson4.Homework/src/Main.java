import model.Book;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Book[] books = new Book[4];

        //gameOfThrones
        Book gameOfThrones = new Book();
        gameOfThrones.title = "Game of Thrones";
        gameOfThrones.author = "George Martin";
        gameOfThrones.publisher = "ACT";
        gameOfThrones.yearOfPublishing = 2016;
        books[0] = gameOfThrones;

        //harryPotter
        Book harryPotter = new Book();
        harryPotter.title = "Harry Potter";
        harryPotter.author = "J.K.Rowling";
        harryPotter.publisher = "SuperPublisher";
        harryPotter.yearOfPublishing = 2012;
        books[1] = harryPotter;

        //lordOfTheRings
        Book lordOfTheRings = new Book();
        lordOfTheRings.title = "Lord of The Rings";
        lordOfTheRings.author = "Tolkien";
        lordOfTheRings.publisher = "Eksmo";
        lordOfTheRings.yearOfPublishing = 2010;
        books[2] = lordOfTheRings;

        //dragons
        Book dragons = new Book();
        dragons.title = "Dance with Dragons";
        dragons.author = "George Martin";
        dragons.publisher = "Bantam Spectra";
        dragons.yearOfPublishing = 2011;
        books[3] = dragons;

        int minYear = books[0].yearOfPublishing;   // Look for the oldest book given.
        int min = 0;
        for (int i = 1; i < books.length; i++) {
            if (minYear > books[i].yearOfPublishing) {
                minYear = books[i].yearOfPublishing;
                min = i;
            }
        }
        System.out.println("Автор самой старой книги: " + books[min].author);
        System.out.println("Введите автора книги: ");
        Scanner scanner = new Scanner(System.in);
        String authorName = scanner.nextLine();

        for (int i = 0; i < books.length; i++) {     //  Look for books of the specific author.
            if (books[i].author.equals(authorName))
                System.out.println("Книга этого автора: " + books[i].title);//
        }
        System.out.println("Введите год: ");      // Look for the book that was published earlier then the given date
        Scanner scanner1 = new Scanner(System.in);
        int year = scanner1.nextInt();
        boolean b = false;
        for (int i = 0; i < books.length; i++) {
            if (year > books[i].yearOfPublishing) {
                books[i].print();
                b = true;
            }

        }
        if (!b) {
            System.out.println("Таких книг нет");
        }

    }
}
