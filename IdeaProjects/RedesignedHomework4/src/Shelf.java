import java.awt.print.Book;

public class Shelf {

    private Book [] books;


    public Shelf() {

        books = generate();
    }

    private Book[] generate() {
        Book [] books = new Book[4];

        books[0] = new Book("Game of Thrones",new String[]{"George Martin"}, "ACT", 2016);
        books[1] = new Book( "Harry Potter",new String[]{"J.K.Rowling"}, "SuperPublisher", 2012);
        books[2] = new Book("Lord of The Rings",new String[]{"Tolkien"}, "Eksmo", 2010);
        books[3] = new Book("Dance with Dragons",new String[]{"George Martin"}, "ACT", 2016);

        return books;
    }

    public void findOldestBook(int yearOfPublishing){   // Look for the oldest book
        for (Book book : books){
            if (book.isTheOldest (yearOfPublishing)) {
                System.out.println(book.getOldestInfo());
            }
        }
    }

    public void findBookWithAuthor (String author){  // Find book of the specific author
        for (Book book : books) {
            if (book.hasAuthor(author)){
                System.out.println(book.getSmallInfo);
                break;
            }
        }
    }
    public void printBookOlderThen(int yearOfPublishing){ // Find books published earlier then the mentioned year
        for (Book book : books){
            if (book.isOlderThen(yearOfPublishing)){
                System.out.println(book.getInfo());
            }
        }
    }
    public void findBooksWithAuthorAndCoauthor(String author){ // Find books written in collaboration
        for (Book book : books) {

            if (book.hasAuthorWithCoauthor(author)){
                System.out.println(book.getSmallInfo);
            }
        }
    }

}
