package model;

public class Book {
    public String title;
    public String [] authors;
    public String publisher;
    public int yearOfPublishing;

   public Book(String title, int yearOfPublishing, String[] author, String publisher) {
        this.title = title;
        this.authors = author;
        this.publisher = publisher;
        this.yearOfPublishing = yearOfPublishing;
    }

    ////////////////////////////
    public boolean isTheOldest (int yearOfPublishing){
        return this.yearOfPublishing>yearOfPublishing;
    }
    public String getOldestInfo(){
        int min = 0;
        for (int i = 1; i < books.length; i++) {
            if (minYear > books[i].yearOfPublishing) {
                minYear = books[i].yearOfPublishing;
                min = i;
            }
        }
        return min + " " + publisher;
    }
    //////////////////////////////
    public boolean hasAuthor (String author){
       for (String auth : authors){
           if (auth.equals(author)){
               return true;
           }
       }
       return false;
    }

    public String getSmallInfo(){
        return yearOfPublishing + " " + publisher;
    }
    ////////////////////////////////

    public boolean isOlderThen (int yearOfPublishing){
        return this.yearOfPublishing>yearOfPublishing;
    }
    public String getInfo(){
        StringBuilder authors = new StringBuilder();
        for (String author : this.authors){
            if (authors.length() != 0){
                authors.append(" ");
            }
            authors.append(author);
        }
        return authors.toString();
    }
    /////////////////////////////
    public boolean hasAuthorWithCoauthor (String author){
        for (String auth : authors) {
            if (auth.equals(author) && authors.length > 1){
                return true;
            }
        }
        return false;
    }


}
