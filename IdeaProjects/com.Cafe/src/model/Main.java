package model;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        HashMap <String, Integer> dishes = new HashMap<String, Integer>();
        dishes.put("Dish One", 500);
        dishes.put("Dish Two", 400);
        dishes.put("Dish Three", 150);
        dishes.put("Dish Four", 200);
        dishes.put("Dish Five", 800);
        dishes.put("Dish Six", 1000);
        System.out.println("Меню ресторана" + dishes);
    }
}
